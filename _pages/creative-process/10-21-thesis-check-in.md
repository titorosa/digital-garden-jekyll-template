---
title: Process Journal Oct 2021
layout: note
---

## first drafts, new ways of writing, world-building 

---

Like dreams & telenovelas, I have trouble with conclusions and finishing things so I wanted to try to write these small check-ins not just to share my progress but also to keep myself on track and have a living documentation of my process.

The last four (or sixish…(or eightish…)) weeks I spent mostly working on an experimental horror novel called Supermutante (alternatively: super0mutante. Alternatively superMUTANTE). It unravels in the form of a wiki. A v rough draft can be found [here](https://super0mutante.netlify.app/).

Many sections still need to be filled out, some deleted altogether, ideas fleshed out, but I finally found something like the “voice” and form of the project (“a hallucinatory rush…”). I’m not sure yet how this narrative will show up in a studio project. Zines will emerge from it I’m sure, maybe some audio/visual pieces, possibly some stickers or posters. Perhaps it won’t show up at all, but I found it steadying to create the “universe” inside which subsequent work will live.

Next I am locking myself up in my studio. I am so lucky because Samia and I just moved in to a very large, very beautiful space and I’m trying to spend some butt-in-chair time (BICTime) in there doing some material thinking and tempo-spatial mutations:

- unlocking and connecting with ambient geotrauma — ???
- collaborating with biomaterials (mycelium sculptures) — ???
- recreating graphic public art projects of the Global South—stickers, wheat paste, collage — ???

Hopefully my next check-in will bring along more new experiments, pleasant dreams, and satisfying telenovela conclusions.

PS.

I have also been drawing some inspiration from a few public artists of the past.


- ELÍAS ADASME, Intervención corporal de un espacio privado. My favorite thing about this one is that Adasme photographed his performances and wheatpasted them around town so it was publicly accessible. > https://artishockrevista.com/2013/11/17/insurgencias-urbanas-estrategias-artisticas-subversivas-espacio-publico-contexto-sudamericano/
- GRACIELA SACCO, Bocanada. The relentless material and spatial infection by the image of the open mouth is a wonderful viral motion  > https://gracielasacco.com/series_de_trabajos/bocanada/
- LYNN HERSHMAN LEESON. The way her works explore identity and technology seems very kin to my project > https://www.lynnhershman.com/project/artificial-intelligence/

