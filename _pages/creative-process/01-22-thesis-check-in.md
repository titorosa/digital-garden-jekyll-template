---
title: Process Journal Jan 2022
layout: note
---

## rough thesis statement, space considerations

---

<div class="image-grid">
	<img src="/assets/img/cpj/fencing-09.jpg">
	<img src="/assets/img/cpj/fencing-10.jpg">
</div>

> I learned to see freedom as always and intimately linked to the issue of transforming space. 
>> --bell hooks

__Research question:__
Studio art and architecture are similar in that they do not mandate a medium or form but consist primarily of space and its transformation. In the case of this thesis, the space is the physical location that makes up the boundary of an exhibition, the site where the addressee receives a work of art. The artist does not just make a mark on a medium but also authors the exhibition site and what is permitted in the space. It is this permitted vs non-permitted that I'm interested in exploring in _Fencing_.  

---

<div class="image-grid">
	<img src="/assets/img/cpj/fencing-02.jpg">
	<img src="/assets/img/cpj/fencing-01.jpg">
</div>

> Another dream: inside, which is outside, a window and me. Through this window I want to pass to the outside, which is the inside for me. When I wake up, the window of my room is the one from my dream; the inside I was looking for is the space outside. 
>> --Lygya Clark

__The space:__
The site I would like to author is the Lazarus building, specifically the space by the window that faces the outside. I am hoping that through the activation of this space and the window that I will also activate the space outside the gallery.

I am thinking of how to do this by piercing the window and allowing the space outside of the gallery into the exhibit. 

The street outside Lazarus is surrounded by signs and structures that mark what is permitted and who is allowed. Badges, keycards, mechanical gates, chain-link fences make up almost the entirety of the block. There is even a chain-link fence to protect the sidewalk in front of a derelict building from the people who had set up camp there. In this way marking can also be a statement of nothingness, the way the fencing blocks out swatches of lifeless space.

A fence is a boundary in space that sits between what is permitted and what is forbidden. What can happen to the space if we bring the fencing inside? 

---

<div class="image-grid">
	<img src="/assets/img/cpj/fencing-12.jpg">
	<img src="/assets/img/cpj/fencing-11.jpg">
</div>

> Being a "non-site" piece, it is not about appearance, but about disappearance.
>> --Lucy R. Lippard, _The Precarious: the Art and Poetry of Cecilia Vicuña_

__How can we make a non-site?__
With this project I am interested in developing techniques for an art that is not about marking but about erasing. An empty space or a void or a door disrupts a surface and implies the space that lies beyond it.

What type of art can be made by erasing a fence? Through the cutting, rending, twisting and weaving of a fence? Can we imply a different type of space through the erasure created by inserting a hole into a fence? 

---

<img src="/assets/img/cpj/fencing-03.jpg">

> The grid is...a re-presentation of everything that separates the work of art from the world, from ambient space and from other objects. The grid is an introjection of the boundaries of the world into the interior of the work.
>> --Rosalind Krauss

__Can an open form lead to an open work?__
By open work I do not mean works such as a novel ([no matter how non-linear its structure](20%20Professional/[21]%20Portfolio/portfolio-site-2/_pages/creative-process/10-21-thesis-check-in.md)) or a painting, which are open in the sense that it is up to the reader or viewer to interpret them in their own way. Instead I use the term in the manner of Umberto Eco--open works are unfinished and require the addressee to work in collaboration with the author to construct the piece. In the case of _Fencing_, I would like to explore how to change the gallery space so that the addressee is invited to collaborate with the work in the form of a fence and wire cutters. These materials would have to be presented in such a way so that it is up to them to impose their judgment on the form of the work, like the location of the hole, its size and how to shape it. 

In this way the work is a visual pun, the physical opening of the fence as metaphor for the opening up of the process of aesthetic production beyond the individual producer. To quote Eco: "[the open work] encourages 'acts of conscious freedom' in collaboration with the author...recasting the work so as to expose it to the maximum possible opening."

Can the work become not about mark making in the modernist sense (the projection of the artist's ego onto the material) but the opposite, erasure? In this way the title is a pun, where the addressee parries and "fences" with the exhibit and its author, acting in negation, erasing the work one snip at a time? 

<!-- 
---
> Flattened, geometrized, ordered, the grid is antinatural, antimimetic, antireal...In the flatness that results from its coordinates, the grid is a means of crowding out the dimensions of the real and replacing them with the lateral spread of a single surface. 
>> --Rosalind Krauss

__Why is the work called _Fencing_?__
What if throughout the gallery exhibit the addressee is surrounded by symbols of negation, such as "Don't touch", "Don't pick up", "Don't cut", "Don't take"? And what if the space clearly invites the addressee to do the opposite of what these symbols mandate? First, through access to wire cutters placed within easy reach of the hand. Second, through the presence of a large wire fence not where we expect it (such as to keep the public out of private property) but where it is not usually found, such as the space within the gallery. Perhaps there are already a few holes cut into the fence, implying that someone has already disobeyed the signs, inviting the current addresee to also disobey this implied author by picking up the cutters and using them to cut a new void into the fence. In this way the work is not about mark making in the modernist sense (the projection of the artist's ego onto the material) but the opposite, the deliberate disobedience of what the author (ostensibly) allows? In this way the work is a pun, where the addressee parries and "fences" with the space and its author, acting against them, in negation to their negations.
 -->
---

<div class="image-grid">
	<img src="/assets/img/cpj/fencing-04.jpg">
	<img src="/assets/img/cpj/fencing-05.jpg">
</div>

> "Prayer" understood not as a request, but as a response, is a dialogue or a speech that addresses what is (physically) "there" as well as what is "not there," the place as well as the "no place," the site as well as the "non-site." Prayer is dialogue as a form of transition from what is to what could be.
>> --Lucy R. Lippard, _The Precarious, the Art and Poetry of Cecilia Vicuña_

__Creating a ( )holey ritual?__

<!-- In _The Negation of the Autonomy of Art by the Avant-Garde_, Peter Burger draws a handy table that outlines the differences in production and reception for sacral art, courtly art and bourgeois art. Sacral art is different in that it is both produced and received collectively, as part of the life praxis of the faithful. In bourgeois art, not just "production but reception also are now individual acts." In this model, the novel rises as the suitable form for bourgeois art, its appropriate mode of reception done by isolated individuals. 
 -->
In _The Negation of the Autonomy of Art by the Avant-Garde_, Peter Burger identifies the difference between bourgeouis art and sacral art in that the former is produced and received by an individual while the latter is produced and received collectively. Taking the cult object as a jumping point, I wonder whether it is possible to generate ( )holey space, a site for the production of non-space? This ( )holey space would serve a specific use, the collective practice of negation. ( )holey altars emit this non-space, where an altar is a manipulation, transformation or erasure of a fenced boundary. The "prayer" or dialogue would begin by mutating in the fence, in an act of symbolic transmission to the transition to what could be beyond. By living outside the ( )holey cult objects would enter into the praxis of everyday life.

 ---


<div class="image-grid">
	<img src="/assets/img/cpj/fencing-07.jpg">
	<img src="/assets/img/cpj/fencing-08.jpg">
</div>

<!-- __How can _Fencing_ be art that is contained within the praxis of everyday life?__
First, the addressee would be primed, through the picking up of the wire cutters and the insistance that they use the tool in the gallery. Signs such as "Don't take outside" wink towards the fact they should in fact take the wire cutter outside. The presence of fences all around the addressee outside, particularly right next to them at the abandoned building, would ideally be all the addressee needs to use their new wire cutters.

However, it is quite likely that no addressee will take the wire cutters with them, nor will they try to activate the fence around the private building, as this may incur a fee or a confrontation with the MICA security force. In that case, the participatory aspect of the piece would become an empty motion, or a spectacle.

An alternative would be to have the fence that is inside the gallery extend "through" the window and into the street. Or perhaps there could be some fence forms (twisted, rent, woven) scattered outside. In this way perhaps the work could draw an invisible line from the fence inside the gallery to the space outside it.

A second alternative would be to include weaving materials as well as wire cutters. These threads could be used to weave into the fence, much in the manner of Cecilia Vicuña. It may be possible to encourage the addressees to take this weaving action outside, as it is a less confrontational form (legally classified as littering vs destruction of property).
---
 -->
<!-- 
> It’s a world where everyone feels on the precipice of a lurking void, so they wander around in an endless quest to transcend the burden of their own awareness.

__Mail art:__
I am also thinking of including an additional element to this piece in the form of goodbye postcards. The front of these postcards are printed with diagrams that depict fences and grids as well as voids, holes, doors and rends. The back of the postcards include quotes and messages as well as links to additional readings. The reason I would like to include these postcards is to provide additional context for the addressee at the gallery and also to expand the scope of the work to my network of collaborators, friends and acquaintances by sending the postcards through the mail. Pens, blank postcards and stamps would also be included for addressees to write and send their own messages. 

Documentation of these postcards can be found [here](https://www.are.na/tito-rosa/postcards-from-labyrinthworld).

 -->
---

__Next steps:__
- Try to build a fence.
- Continue exploring different ways to activate fencing in public places. Install some more ( )holey altars.
- Develop a ritual for collective fence erasure?

---

__Inspo:__
- [Fences of KTown](https://twitter.com/FencesOfKtown). This Twitter account is a collective work that documents illegal fencing in the Koreatown LA area and serves as a space for discussing and organizing for the rights of the unhoused.
- [_Good Fences make Good Neighbors_](https://www.archpaper.com/2017/10/ai-weiwei-good-fences-good-neighbors/#gallery-0-slide-0) by Ai Weiwei. I really appreciate the way Weiwei utilizes the form of the fence to create public art. However, most everyone interacts with these pieces by taking photos of them and so for me this does not qualify as an "open work."
- [_No-Stop City_](https://www.frac-centre.fr/_en/art-and-architecture-collection/archizoom-associati/no-stop-city-317.html?authID=11&ensembleID=42) by Archizoom. This work has been a constant source of reference for me. I am inspired by the way the Archizoom collective takes a structure of control (in this case the architectural mega-structures) and stretches to infinite in order to critique the existing power structure as well as point to freedom beyond. I wonder what a _No-Stop Fence City_ would look like?

<img src="https://2.bp.blogspot.com/_tpF2_7rzFMU/SRDLPtqpKTI/AAAAAAAABUk/_-m0axHg-28/w1200-h630-p-k-no-nu/nsc08.jpg" />


