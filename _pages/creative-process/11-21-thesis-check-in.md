---
title: Process Journal Nov 2021
layout: note
---

## new directions, unsubscribing as a motion

---

During the last [[Process Journal Oct 2021|check-in]] I wrote about my process writing the first embryo draft of an interactive horror story called Supermutante. Writing out that draft served to creatively stake out the universe of topics I was drawn to: power, neocolonialism, revolution, indigenous knowledge & the Global South, and where those topics intersect with technology, information, contamination, and mega-structures. All of these ideas swirled around the protagonist of the novel, Supermutante, the world's first hybrid human-computer virus.

After sharing out this first draft, I mentioned my excitement towards creating physical manifestations of this virus in the world. My mentor Fabienne thought it might be fruitful "to conjure a motif already found 'naturally' in the world, the recognition of which depends on a shift in focus, or a newly found obsession" as a form of irl spatial infection of sorts.

I began to visit the Lazarus gallery to study the studio space where the exhibition would live. The university is an institution I have a complicated relationship with. On the one hand I am enrolled there and benefit from interacting with the tools, technologies and professors there. On the other hand, the administration and its private security force has been a constant antagonist during my time here. Everyone I know has a story about harrassment from the security guards, including myself. Even worse, on my last visit, I noticed that the university and the city had evicted a groupd of houseless artists who were camping in front of a dilapitated building down the block. Rather than connect these artists to community resources, they were forcefully removed. And to protect an abandoned building? They erected a big fence.

How could I lend my creative capital to an institution that behaved this way? Would showing my work there tacitly endorse institutional actions that have caused great suffering to the community?

These questions sucked a lot of my creative momentum and I did nothing for quite a while. If I didn't want the university to take free creative capital to advance its own esurient project, what could I show there? Would it be possible to show nothing at all? Could I make myself, and my art, into something that couldn't be institutionally co-opted?

I began to make myself disappear. First I made business cards. Not for introducing myself, but rather to say goodbye.

<img src="/assets/img/goodbye-card-1.jpg">

Looking back, I found that this motion, that of voiding, rose to the surface as soon as I started my degree. My first year I removed the artist completely and had a cheeky AI do my presentation for me. My second year I dove deeper by curating an [Unsubscribe Gallery](https://www.are.na/tito-rosa/unsubscribe-kvcl3qtkkmy) of different unsubscribe motions, from signing out of Facebook to turning down awards to exiting existence altogether.

I found this [Unsubscribe Gallery](https://www.are.na/tito-rosa/unsubscribe-kvcl3qtkkmy) to be fertile creative ground for irl constructions (not objects exactly, more like what Lygia Clark meant when she dubbed her objects _bichos_--bugs). As soon as I started, I began to see voids and unsubscriptions everywhere I went. I realized I even had started incorporating imaginery of negative space invading fencing into a series of [postcards](https://www.are.na/tito-rosa/postcards-from-labyrinthworld).

Perhaps I found the naturally occuring motif my mentor alluded to--the void.

MONTHLY INSPO:

-   Eugene Thacker, particularly their writing on the rich history of 'negative mysticism' (apophasis) in the horror philosophy book Starry Speculative Corpse. Apophasis is the rhetorical strategy of approaching a subject by denying its existence, or denying that it can be described. "The foundational apophatic writer in the Christian mystical tradition was Dionysius the Areopagite, who stated in the fifth or sixth century that only 'by knowing nothing, one knows beyond the mind.' Six centuries later, Meister Eckhart, influenced by Dionysius and likely by Porete, described God as the 'negation of the negation.'"
-   Lee Lozano, particularly _General Strike_, where she documented her final appearences in the art world, and _Dropout Piece_, where she retreated entirely from her art practice.
