---
layout: exhibit
title: MICA Grad Show
gallery_name: Maryland College Institute of Art
gallery_url: https://2022.micagradshow.com/details/recm2eEAjdI3IyNNK
show_date: June 22nd – July 9th, 2022
images:
    - /assets/images/MICA-grad-show/MICA-grad-show-01.jpg
    - /assets/images/mutantechitecture/01_titorosa_mutantechitecture-i_2022.jpg
    - /assets/images/mutantechitecture/07_titorosa_mutantechitecture-i-iii_2022.jpg
    - /assets/images/mutantechitecture/02_titorosa_mutantechitecture-ii_2022.jpg
    - /assets/images/mutantechitecture/03_titorosa_mutantechitecture-ii_2022.jpg
    - /assets/images/mutantechitecture/04_titorosa_mutantechitecture-iii_2022.jpg
    - /assets/images/mutantechitecture/05_titorosa_mutantechitecture-iii_2022.jpg
    - /assets/images/mutantechitecture/06_titorosa_mutantechitecture-iii_2022.jpg
---

<hr/>
<h3 class="uppercase">Works</h3>
<p><a class="internal-link" href="/mutantechitecture-i-iii">Mutantechitecture I</a></p>
<p><a class="internal-link" href="/mutantechitecture-i-iii">Mutantechitecture II</a></p>
<p><a class="internal-link" href="/mutantechitecture-i-iii">Mutantechitecture III</a></p>
<hr/>
