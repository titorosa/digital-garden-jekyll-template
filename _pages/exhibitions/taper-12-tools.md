---
layout: exhibit
title: Taper 12 - Tools
gallery_name: Taper Magazine
gallery_url: https://taper.badquar.to/12/
show_date: May 2024
images:
    - /assets/images/publications/taper-12-01.png
    - /assets/images/publications/taper-12-02.png
---

<hr/>
<h3 class="uppercase">Works</h3>
<a class="internal-link" href="/extremely-constrained-notepad">Extremely Constrained Notepad</a>
<hr/>

Toy box if not hefty tool chest, issue #12 of Taper offers 32 poems — programs — systems — for your delectation, play, and use in response to the theme Tools.

Faced with a problem, the human reaches for a tool: an adze to hollow a tree, a clock or a calendar to measure time, a spell checker to polish prose. We invite submitters to test how much usefulness can be packed into a minimalist digital tool; can the bloated software suites we all rely upon—photo editors, word processors, calendar apps—be effectively miniaturized? How might a small piece of software help the “end user” to think or sleep more deeply? How might it, as an instrument or palette, stimulate the player’s creativity? What tedious tasks should be automated? We welcome poems, artworks, games, and gadgets that ponder or make mischief with the very notion of tools and usefulness.
