---
layout: exhibit
title: Offsite_01
gallery_name: Sleepwalker Collective
gallery_url: https://linktr.ee/sleepwalker_collective
show_date: October 22, 2023
images:
    - /assets/images/mutantechitecture/mutantechitecture_outdoor_1.webp
    - /assets/images/mutantechitecture/mutantechitecture_outdoor_4.webp
---

<hr/>
<h3 class="uppercase">Works</h3>
<a class="internal-link" href="/mutantechitecture-iv">Mutantechitecture IV</a>
<hr/>

Offsite 1 is an alternative art exhibition. The offsite format involves artworks being installed at non-gallery sites such as parks and abandoned buildings. We're interested in what happens when we move out of the familiar institutional-industrial space and get a little feral. Do we achieve symbiosis? Will we and the space we occupy for this moment become one or will we become remarkably fantastically awkward in our attempt to meld? With audacity as our guide, we celebrate the untamed, the unstructured, and the enchantingly wild.

The goal of this event is to engage with different environments and connect with audiences who might not typically visit a formal art institution.

We aim to foster community for artists in Baltimore by pushing the boundaries of where and how art can exist and function. We believe that Art can be anywhere and anything by anybody.
