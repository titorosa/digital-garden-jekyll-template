---
layout: exhibit
title: Taking Space
gallery_name: Creative Alliance
gallery_url: https://creativealliance.org/event/taking-space-exhibition/
show_date: September 1 - October 21, 2023
images:
    - /assets/images/mutantechitecture/mutantechitecture_mobile_1.webp
    - /assets/images/mutantechitecture/mutantechitecture_mobile_2.webp
    - /assets/images/mutantechitecture/mutantechitecture_mobile_4.webp
---

<hr/>
<h3 class="uppercase">Works</h3>
<a class="internal-link" href="/mutantechitecture-v">Mutantechitecture V</a>
<hr/>

Your voice is valid, and we need it in this world. Taking Space is an exhibition that celebrates Latino artists based in Baltimore expressing their love of their cultures and elevating the conversation of their cultures. Each artist has a unique perspective of the Latino community, or Latinidad, and their individual stories of hope, happiness, and history. The artists come from different backgrounds and experiences. Each artist will showcase painting, sculpture, and other mediums to dig deeper into the cultural diversity of Hispanic Heritage Month.
<br />
<br />
Taking Space usually refers to the need to separate, but the theme of this exhibition is coming together and centering multiple identities and experiences. The exhibition features the work of Agustin Rosa, Christina Delgado, Jessy DeSantis, Jaz Erenberg, and Edgar Reyes. Through an onsite artist talk and offsite presentations, the artists will be dialoguing with themselves and the public to bring you their vision.
