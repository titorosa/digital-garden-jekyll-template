---
layout: project
show_home: true
title: Object of My Affection
description: custom phone cases for material speculation
created_at: 2021
materials: 3-D printed PLA, acrylic, smartphone
size: variable
thumbnail: assets/images/object-of-my-affection/thumbnail.jpg
images:
    - /assets/images/object-of-my-affection/final-1.jpg
    - /assets/images/object-of-my-affection/final-2.jpg
    - /assets/images/object-of-my-affection/final-3.jpg
    - /assets/images/object-of-my-affection/final-4.jpg
---

Few things are more intimate than our smartphones, an artificial object we cradle, speak into, hold up to our faces and even fall asleep with. Yet most of these physical interactions are engineered by consumer culture technicians. _Object of My Affection_ is a collection of custom cases for smartphones that invite the viewer to touch in playful new ways. By remeshing the phone, the case becomes a site of material imagination, a speculative and theatrical prosthetic. Touch is unique because of the fluidity we feel with the touched object, the object touching back, undrawing the boundaries between self and other.
