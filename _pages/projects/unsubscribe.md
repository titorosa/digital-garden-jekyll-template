---
layout: page
title: Unsubscribe Gallery
---

---

## open-source digital curation project

#### 2020

<img src="/assets/img/unsubscribe/unsub-1.png">

Unsubscribe Gallery can be found [here](https://www.are.na/tito-rosa/unsubscribe-kvcl3qtkkmy).

The gallery is an open-source collection of zines, bots, images and unclassifiable digital artifacts that generate, examine, relish and critique the motion of unsubscription.

One of my positive obsessions is collecting unsubscription forms. I started in the summer of 2020:

-   I unsubscribed from spam emails
-   I did all 6 unsubscription steps for shutting down my Facebook account
-   I unsubscribed, resubscribed and unsubscribed from Twitter.
-   Someone handed me a RENT STRIKE poster and I thought that too was a motion of unsubscription.
-   I subscribed to an are.na account and started collecting...

At the bottom of the Gallery sits a collective piece [You Have Been Signed Up](https://join-unsubscribe.glitch.me/), an interactive poem that allows users to add an unsubscription option to an ever-expanding sign up form. What did you never sign up for that you would like to click unsubscribe on?

---

<div class="image-grid">
	<img src="/assets/img/unsubscribe/unsub-2.png">
	<img src="/assets/img/unsubscribe/unsub-3.png">
	<img src="/assets/img/unsubscribe/unsub-4.png">
</div>
