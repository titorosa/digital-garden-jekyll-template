---
layout: note
title: its alive and dead and so it cant be wrong
---

*it's alive and dead and so it can't be wrong* is a multi-media horror story about a mysterious gas cloud that devours people, objects and cities whole. Over the course of 1,000 years the world ends, then ends again. People are slaughtered but also hope is found. Told by an eerie, shifting voice--sometimes human, sometimes not--the narrative records panic, pain and ecstasy at the edge of the apocalypse.

The video was created patchwork, using algorithmic writing and automated voice synthesis to explore how we build individual and collective mythologies and counter-mythologies. The word is sound, the sound the language, the noise the language, the language the world. All of it gets patched together through a pattern of partial and contradicting realities to form a hallucinatory vision of escape to a ruined future.

What does it feel like for the world to end when the world is ending all the time? The lonely drives, the falling, the isolation, the noise and the life of all the people the world will never have. How should we mourn this world, exactly? How do we miss a world we need to dismantle?

<i>You are alone in a vast and ruined land, lying under a blackened sky. War and blight have fallen upon us. Words were lost, but music remained, and the people embraced that. No one had died yet, all of our comrades were dancing around us.</i>
