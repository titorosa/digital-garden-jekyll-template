---
layout: project
title: Good Moral Standing (Green Card)
description:
created_at: 2023
materials: digital prints
size: 5' x 7'
images:
    - /assets/images/green-card/green-card-210.png
    - /assets/images/green-card/green-card-213.png
    - /assets/images/green-card/green-card-214.png
    - /assets/images/green-card/green-card-215.png
    - /assets/images/green-card/green-card-216.png
    - /assets/images/green-card/green-card-217.png
---

<i>Good Moral Standing (Green Card)</i> mines the language used in real deportation proceedings to explore the way immigration courts serve as sites for the perpetuation of national identity myths. A 'green card' is a document that proves a person has permanent residency in the U.S. The law mandates that those seeking permanent resident status be able to demonstrate they are in "good moral standing" with the state. Despite their ostensibly neutral standards, immigration courts often use the "moral standing" clause to give sanction to nostalgic, idealized and exclusionary images of American identity.

This project consists of a series of cards, each one with a written imperative for how an asylum-seeker might come to fall under "good moral standing" within the law. Each card also depicts a green square which varies in size, rotation and position card to card, symbolizing the circuitous and unpredictable nature of the process of trying to acquire legal immigration status. The narrative threads that bind these cards together also frays at the edges, inviting contemplation on the power of language to construct, distort, and define narratives of identity and subordination.
