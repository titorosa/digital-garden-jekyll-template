---
layout: project
show_home: false
title: Lets Go Somewhere Else
description: pop-up horror story
created_at: 2019
materials: webpage
size:
thumbnail: assets/images/lets-go-somewhere-else/lets-go-1.png
images:
    - /assets/images/lets-go-somewhere-else/lets-go-1.png
    - /assets/images/lets-go-somewhere-else/lets-go-2.png
    - /assets/images/lets-go-somewhere-else/lets-go-3.png
    - /assets/images/lets-go-somewhere-else/lets-go-4.png
---

<!-- _Exhibited at Fulton Street Collective Revolucion Pop Up Art Show_ -->

<video controls="controls" width="800" name="Let's Go Somewhere Else" src="/assets/images/lets-go-somewhere-else/lets-go.mov"></video>

Interactive version can be found <a href="https://deadalivemagazine.com/vol1/lets-go-somewhere-else.html">here</a>
