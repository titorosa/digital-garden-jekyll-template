---
layout: note
title: Labor of Love
---

## automated resignation letter generator

#### 2021

---

<img src="/assets/img/labor-of-love/labor-of-love-03.jpg">

_(Dedicated to Zlata & Leah, who have at different times in my graduate career lent me very patient ears. Best of luck on your resignation and retirement)_

**Research questions:**

-   What can ambient literature look like?
-   How can we use electronic literature to explore the potentials and limits of labor refusal?

**The process:**
_Labor of Love_ is an infinite resignation letter. The resignation letter is authored via a series of automated processes. First, a small bot posts a fragment of the letter on [Twitter](https://twitter.com/sampleresignat1). Each time a new fragment is posted, a small computer attached to a receipt printer prints it out on a roll of thermal paper. As time passes the resignation letter coils out of the machine, ribbons on the floor in strands 50 feet long.

<img src="/assets/img/labor-of-love/labor-of-love-02.jpg">

**Automatic writing:**
Although the text in _Labor of Love_ may seem like the product of an artificial intelligence or another complex algorithm, it is actually an extremely simple script that randomly chooses from a series of pre-determined phrases. The generator leverages a well-known phenomena in electronic literature known as the [ELIZA Effect](https://en.wikipedia.org/wiki/ELIZA_effect), where humans will "assume that [machine outputs] reflect a greater causality than they actually do."

The resignation letter as a format is ideal for automation. It is a form that's highly structured, ceremonious in tone and follows a standard template. Most of the phrases in a resignation are compiled not with emotion but rather as a series of machinic social obligations.

<img src="/assets/img/labor-of-love/labor-of-love-04.jpg">

**Infinite literature:**
The _Labor of Love_ seizes on the semi-meaningless rhetoric of the resignation and takes it to an extreme by stretching it to (implied) infinity. The language becomes meaningless and ambient, reflecting the hollow core of the relationship between overworked and underpaid workers and the management/ownership class. Yet when pausing at any one section of the letter (a point within a line) one can also sense a determined and defiant feeling of negation.

> Smooth space...[is] where variation and development of form are continuous and unlimited...Smooth space is filled by events...Whereas in the striated space forms organize a matter, in the smooth space materials signal forces and serve as symptoms for them. It is an intensive rather than extensive space, one of distances, not of measures and properties.
>
> > -- Gilles Deleuze and Felix Guattari

**Infinite labor:**
A resignation is a way of saying goodbye, dissolving a relationship. Quitting your job can mean the beginning of a new phase in life, the release from difficult or unpleasant working conditions. Yet without access to social protections or community resources, saying goodbye to a job simply means saying hello to a new one, on and on. A parade of jobs and their corresponding resignation letters. Because the generator's letter never ends, it can only hint towards a threshold beyond work, a threshold that becomes a horizon that can never be reached. Quitting is both an act of resistance and futile one. One can never fully [[20 Professional/[21] Portfolio/portfolio-site-2/_pages/projects/unsubscribe]].

For a lot of workers, even resignation is not possible. Quitting lives as a fantasy or in small acts of resistance, such as stealing bits of company time or property. The resignation letter in the generator veers subtly into fantasy, with phrases such as "I do not wish to come in today" that flirt with wish fulfillment, putting into writing what we often can't say in a professional setting.

Viewed this way, the generator becomes a visualization of form over time, the compilation of a lifetime of cyclical labor extraction. To the narrator trapped in this infinite letter, what do you feel? Hopeful? Vengeful? Or desperate?

<img src="/assets/img/labor-of-love/labor-of-love-01.jpg">
