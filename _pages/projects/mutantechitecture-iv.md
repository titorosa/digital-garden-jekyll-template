---
layout: project
show_home: true
title: Mutantechitecture IV
description: shape-shifting outdoor sculpture
created_at: 2023
materials: bambu, string, acrylic
size: 6' x 5' x 8'
thumbnail: assets/images/mutantechitecture/mutantechitecture_outdoor_thumb.png
images:
    - /assets/images/mutantechitecture/mutantechitecture_outdoor_1.webp
    - /assets/images/mutantechitecture/mutantechitecture_outdoor_4.webp
---

<!-- <p>This is how it spreads: as you move inside the mutante you enter into some new type of symbiosis. The materials self-organize into a precarious architecture. Without warning you become responsible for the process of spatial distribution for this new object-organism. In exchange the object folds and unfolds at your touch, revealing new configurations</p>
<p>I began to create sites, shape-shifters, resembling makeshift structures that after a while seemed to emerge from the very nature of what it means to exist and move within the material flow. Though it is an unpredictable building, in truth mutantechitectures can easily bend or snap. They can be untied and retied, put together or taken apart. They are waterproof but susceptible to wind. Mutantechitecture comes together in spontaneous, improvised motions that involve the hands, the neck, the legs and arms.</p>
<p>If energized mutantechitecture enters into dynamic partnership with the body, the ground, its own network of material pull and give. Tension extends in all directions to create networks of forces that counterbalance each other. If tire the mutantechitecture is left to rest, in that moment of rest emerges an inconclusive configuration, always in the making, prone to unexpected form, at the edge of becoming something different. It stays always within the interval, the in-between. Where does the mutante end and I begin?</p> -->
