---
layout: project
show_home: false
title: Extremely Constained Notepad
description: word processor with amnesia
created_at: 2024
materials: Interactive website, single-channel video
size: variable
thumbnail: assets/images/extremely-constrained-notepad/thumbnail.png
images:
    - /assets/images/extremely-constrained-notepad/thumbnail.png
    - /assets/images/extremely-constrained-notepad/final-01.png
---

I wanted to build a useless tool, a word processor with amnesia. What new forms can arise from this constraint? What kind of writing can thrive in a program that has a memory, not like a machine's memory, infinite and unchanging, but like a human's, fallible and ephemeral?

The Extremely Constrained Notepad allows you to type 20 characters at a time, deleting the first character as you type the 21st.

This tool has proven particularly suited for constrained writing exercises and livewriting performances. For composing poems that are too long to remember, but too short to write down. Ambient poems not to be read but looked at. It is portable, simple, and easy to extend with custom functionality. I look forward to see what the community will create with it.

In order to use this tool, start typing. The ECN has a repeat mode, which when toggled will repeat the last key you pressed until you press a new key. Repeat mode allows you to create aaaammbbiieennttt pppppoooeeeemsss llliikkeee ttthhiiisss.

<video controls="controls" width="800" name="ECN-01" src="/assets/images/extremely-constrained-notepad/ECN-01.mov"></video>
<video controls="controls" width="800" name="ECN-02" src="/assets/images/extremely-constrained-notepad/ECN-02.mov"></video>
<video controls="controls" width="800" name="ECN-03" src="/assets/images/extremely-constrained-notepad/ECN-03.mov"></video>
<video controls="controls" width="800" name="ECN-04" src="/assets/images/extremely-constrained-notepad/ECN-04.mov"></video>

## Interactive

-   <a href="https://taper.badquar.to/12/extremely_constrained_notepad.html">Original publication (Taper Magazine)</a>
-   <a href="/extremely-constrained-notepad-interactive">Cannonical version</a>
