---
layout: project
show_home: true
title: Mutantechitecture I-III
description: kinetic play mobiles
created_at: 2022
materials: bambu, string, acrylic
size: variable
thumbnail: assets/images/mutantechitecture/07_titorosa_mutantechitecture-i-iii_2022.jpg
images:
    - /assets/images/mutantechitecture/01_titorosa_mutantechitecture-i_2022.jpg
    - /assets/images/mutantechitecture/07_titorosa_mutantechitecture-i-iii_2022.jpg
    - /assets/images/mutantechitecture/02_titorosa_mutantechitecture-ii_2022.jpg
    - /assets/images/mutantechitecture/03_titorosa_mutantechitecture-ii_2022.jpg
    - /assets/images/mutantechitecture/04_titorosa_mutantechitecture-iii_2022.jpg
    - /assets/images/mutantechitecture/05_titorosa_mutantechitecture-iii_2022.jpg
    - /assets/images/mutantechitecture/06_titorosa_mutantechitecture-iii_2022.jpg
---
