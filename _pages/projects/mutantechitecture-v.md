---
layout: project
show_home: true
title: Mutantechitecture V
description: kinetic play mobile
created_at: 2023
materials: bambu, string, acrylic
size: 7' x 3' x 4'
thumbnail: assets/images/mutantechitecture/mutantechitecture_mobile_thumb.png
images:
    - /assets/images/mutantechitecture/mutantechitecture_mobile_1.webp
    - /assets/images/mutantechitecture/mutantechitecture_mobile_2.webp
    - /assets/images/mutantechitecture/mutantechitecture_mobile_4.webp
---

(Mutante + Architecture)
<br/>
<br/>
I began to create sites, makeshift structures, shape-shifters. Made of bambu and string, these improvised forms can be untied and retied, put together or taken apart. Mutantechitecture comes together in spontaneous motions that involve the body, floor, ceiling and light. The materials self-organize into a precarious, site-specific architecture.
<br/>
<br/>
You enter into a type of symbiosis: The object folds and unfolds at your touch, revealing new configurations. If energized mutantechitecture enters into dynamic partnership with the body, the ground, its own network of material pull and give. Tension extends in all directions to create networks of forces that counterbalance each other.
<br/>
<br/>
If tired the mutantechitecture is left to rest, in that moment of rest emerges an inconclusive configuration, always in the making, prone to unexpected form, at the edge of becoming something different. It stays always within the interval, the in-between. Where does the mutante end and I begin?
