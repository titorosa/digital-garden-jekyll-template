---
title: about
layout: note
---

I am an editor, writer and object maker from Rosario, Argentina currently residing in Baltimore, Maryland. I run [Dead Alive](deadalivepress.com), an experimental press that explores the intersection of storytelling and technology.

I utilize speculative prosthetics, interactive objects, printed matter, digital exhibitions and community happenings to create participatory ambiences that exist somewhere between object, space and ritual.

My work is often presented unfinished, requiring the addressee to work in dynamic partnership with artist and material. In an open work the role of artist, audience and object are in constant renegotiation. This generates a dynamic flow, a coupling and intermingling of events, causes and materials, that envisions transformation as a collective activity. Everything connects and leaks.

In this affective reconfiguration, inside, outside, space and audience are continuously metamorphosed through action. New words, gestures, postures and social relations arise. Subject and object blend into one another through interaction and collaboration. Whatever we touch touches us back.
