---
title: Two Love Poems
layout: page
---

## thismuch.org

### 2023.02

---

_*Archive*_

[LINK](https://www.thismuch.org/2023)

<img src="/assets/img/publications/thismuch-love-poems/thismuch-1.png">
<img src="/assets/img/publications/thismuch-love-poems/thismuch-2.png">

1.

I love you so much that I carry
you in my mind
wherever I go

I only have to unhinge my jaw
to hear your voice

and when I need to see the world
like you see the world
I just pop
each of my eyes
and let you peer out

when I tear back
the upper part of
the scalp
and reach in I can feel
that little simulated you that's grown

along the inner curve
of my skull
like a tendril

2.

1 lover's heart
3 cloves of garlic, minced
1 tablespoon of fresh rosemary
1 tablespoon of butter
salt and pepper to taste

Rinse the heart under cold water
and pat dry with a towel

In a large pot, heat the butter
add the minced garlic and fresh herbs
sauté until fragrant

Place the heart in the pot
sear all sides until golden brown.

Lower the heat
let the heart sit until tender
serve hot
but remember it is polite
to eat every bite
but the last
